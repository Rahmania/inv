$( document ).ready(function() {

	function showAlert(txt)
	{
		$(".alert-to-copy").clone().appendTo("#alert-container").removeClass("alert-to-copy hide").append(txt);
	}

	function getURLParameter(sParam)
	{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');

		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam)
				return sParameterName[1];
		}

		return null;
	}
	
	function getToken()
	{
        var login = { username: "admin", password: "admin" };
        var posting = $.post("http://212.201.64.150/api/v1/Login/getToken", login, function() {}, "json");
		posting.done(function(data) {
			//document.location = '?token=' + data.result.token;
			var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?token=' + data.result.token;
			window.history.pushState({path:newurl},'',newurl);
		});
        posting.fail(function() {
            alert('Receiving token failed.');
        });
	}
	getToken();

	function addInvoice(data, attempts)
	{
		var number = 1 + Math.floor(Math.random() * 10);
		data['number'] = number;
		var posting = $.post("http://212.201.64.150/api/v1/Invoice/addInvoice", data, function() {}, "json");
		posting.done(function() {
			$('#ld_export').addClass('hide');
			console.log("Invoice with number '"+number+"' done...");
			//alert("Invoice done! Number: "+number);
		});
		posting.fail(function(jqXHR, textStatus, errorThrown) {
			console.log("err: "+errorThrown);
			if (errorThrown == 'Unauthorized')
			{
				$('#ld_export').addClass('hide');
				//alert('Unauthorized');
				showAlert("<b>Error:</b> Unauthorized...");
			}
			else
			{
				if (attempts >= 100)
				{
					$('#ld_export').addClass('hide');
					//alert('Too many attempts!');
					showAlert("<b>Error:</b> Too many attempts! Export interrupted...");
				}
				else
				{
					attempts = attempts + 1;
					console.log("Invoice with number '"+number+"' failed... trying again... " + attempts);
					addInvoice(data, attempts);
				}
			}
		});
	};

    $('#btn_export').click(function() {
			$('#ld_export').removeClass('hide');
	
			var data = {};
			data['token'] = getURLParameter('token');
			
			var d = new Date();
			data['date'] = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
			
			data['discountrate'] = 0;
			data['description'] = "";
			data['discountdate'] = "2012-03-23 17:45:12";
			data['duedate'] = "2012-03-23 17:45:12";
		
			var pos_desc = [];
			var pos_quan = [];
			var unitprice = [];
			var vat = [];
			$('.edit-table').each(function() {
				pos_desc.push($(this).find("#desc").text());
				pos_quan.push(parseInt($(this).find("#quan").text()));
				unitprice.push(parseFloat($(this).find("#unit").text()));
				vat.push(parseFloat($(this).find("#vat").text()));
			});
			
			data['pos_description'] = pos_desc;
			data['pos_quantity'] = pos_quan;
			data['pos_unitprice'] = unitprice;
			data['pos_unit'] = ["empty"];
			data['vat'] = vat;
			
			data['addressid'] = 1;
			data['name'] = "";
			data['street'] = "";
			data['city'] = "";
			data['zip'] = "";
			data['country'] = "";
			
			console.log(data);
			addInvoice(data, 0);
    });
});